import random


def rock_paper_scissors(all_choices, user_win_choices):
    compt_choice = random.choice(all_choices)
    user_choice = input("Enter your choice : ").lower()
    if user_choice not in all_choices:
        return "Enter the correct choice"

    print(f"computer choice : {compt_choice}")
    
    if user_choice == compt_choice:
        return "It's Draw"
    else:
        compt_index = all_choices.index(user_choice)
        if compt_index:
            if user_win_choices[compt_index] == "paper":
                return "You Win"
            else:
                return "You Lost"

        if compt_index:
            if user_win_choices[compt_index] == "scissors":
                return "You Won"
            else:
                return "You Lost"
        
        if compt_index == 2:
            if user_win_choices[compt_index] == "rock":
                return "You Won"
            else:
                return "You Lost"

all_choices = ["rock", "paper", "scissors"]
user_win_choices = ["paper", "scissors", "rock"]
user_loose_choice = ["scissors", "rock", "paper"]
start = True

while start:
    result = rock_paper_scissors(all_choices, user_win_choices)
    print(result)
    print("""
    Do you want to try again ??
    press 1 to continue 
    press 2 to exit
    """)
    cont_or_ex = int(input("Enter no.: "))
    if cont_or_ex == 2:
        start = False


    
