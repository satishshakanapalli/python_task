class Student:
    def __init__(self):
        pass
    def __str__(self):
        return f"{self.name}\t\t {self.tel}\t\t {self.eng} \t\t {self.mat} \t\t {self.chem} \t\t {self.phy} \t\t {self.total} \t\t {self.roll_number} \n"

    def get_marks_input(self):
        self.tel = float(input("enter Telugu marks: "))
        self.eng = float(input("enter English marks: "))
        self.mat = float(input("enter Maths marks: "))
        self.phy = float(input("enter Physics marks: "))
        self.chem = float(input("enter Chemistry marks: "))

    def get_name_input(self):
        self.name = input("Enter student name: ")
    
    def cal_total_marks(self):
        self.total = (self.tel + self.eng + self.mat + self.phy + self.chem)
        return self.total

    def get_roll_number(self, roll_num):
        self.roll_number = roll_num
        return self.roll_number
    
    # def student_dict(self):
    #     return {
    #         "Telugu": self.tel,
    #         "English" : self.eng,
    #         "Maths": self.mat,
    #         "Physics": self.phy,
    #         "Chemistry" : self.chem
    #     }

    
class Class:
    def __init__(self, students: list[Student] | None = None):
        self.students = []
        if students:
            if isinstance(students, list):
                for student in students:
                    if isinstance(student, Student):
                        self.students.append(student)

    
    def __str__(self):
        heading = "Name\t\t Telugu\t\t English \t Maths \t\t Chemistry \t Physics \t Total_marks \t Roll_Number\n"
        for student in self.students:
            heading += str(student)
            heading += "\n"
        return heading
    
    def find_by_name(self, name: str):
        if isinstance(name,str):
            for student in self.students:
                if student.name == name:
                    return student
                else:
                    return f"{name} not present in the list."
        else:
            raise (f"Name should be string but it is {type(name)}")
    def find_by_roll_number(self, number):
        if isinstance(number, int):
            for student in self.students:
                if student.roll_number == number:
                    return student
        
        else:
            raise TypeError(f"Roll number should be an integer but it is {type(number)}")


students_list = []
for i in range(2):
    # obj = input()
    obj = Student()
    obj.get_name_input()
    obj.get_marks_input()
    print(obj.cal_total_marks())
    roll_num = i + 1
    obj.get_roll_number(roll_num)
    students_list.append(obj)

my_students = Class(students_list)
print(my_students)

# stud_name = input("search with student name: ")
# print(my_students.find_by_name(stud_name))

# number = int(input("enter_needed roll number: "))
# print(my_students.find_by_roll_number(number))

print("""How you want call student marks details 
        press 1 by name
        press 2 by roll_number
        """)
press_num = int(input("enter no:"))

if press_num == 1:
    stud_name = input("Enter student name: ")
    print(my_students.find_by_name(stud_name))   
else:
    number = int(input("Enter roll number: "))
    print(my_students.find_by_roll_number(number))









       