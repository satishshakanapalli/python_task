import sys

def copy_file(source, destination):
    '''
    this simple function is for copy contents from one file
    to another file using args.
    '''
    with open (source, "r") as source_file:         # <--- Opening source file
        with open(destination, "w") as destination_file:     # <--- Opening destination file
            contents = source_file.read()           # <--- Reading source file contents
            destination_file.write(contents)  # <--- Rriting content from the source file


source = sys.argv[1]         # <--- Source file name from the args 
destination = sys.argv[2]      # <--- Destination name file from the args 
copy_file(source, destination)  # <--- Calling the function to copy file