#max anf min numbers in a list

def max_and_min_num_in_input_list(input_list, length):
    max_num = input_list[0]                  #let's assume first item as maximum number
    min_num = input_list[0]              #let's assume first item as minimum number
    
    for i in range(1, length):
        if max_num < input_list[i]:
            max_num = input_list[i]
            
        if min_num > input_list[i]:
            min_num = input_list[i]
    return max_num, min_num

input_list = [1,2,3,4,5,6,7,8]  
len_of_input_list = len(input_list)
maximum_number, minimum_number = max_and_min_num_in_input_list(input_list, len_of_input_list)
print(f"{maximum_number} is the maximum number")
print(f"{minimum_number} is the minimum number")