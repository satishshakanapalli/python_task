def sentence_split(sentence, split_char):
    word = ""
    words_list = []
    for each_char in sentence:
        if each_char == split_char:
            words_list.append(word)
            word = ""
            continue
        word = word + each_char
    words_list.append(word)     #for the last word in a list
    return words_list
    
sentence = "sagar is a good is a boya multiple"
split_char = "a"

split = sentence_split(sentence, split_char)
print(split)
