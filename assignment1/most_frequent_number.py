# most frequent number from the given list

def max_and_min_num_in_input_list(input_list, length):   #max and min funtion
    max_num = input_list[0]                  
    min_num = input_list[0] 
    
    for i in range(1, length):
        if max_num < input_list[i]:
            max_num = input_list[i]
            
        if min_num > input_list[i]:
            min_num = input_list[i]
    return max_num, min_num

def most_repeat_num(list_a):
    dict_a = {}
    for i in list_a:             # for getting each number
        count = 0
        for j in list_a:        # inner loop for counting the number
            if i == j:
                count = count + 1
        dict_a[i] = count      #storing in dictionary
    print(dict_a)
    num_list = []    #keys list
    val_list = []    #values list
    
    for num, val in dict_a.items():
        num_list.append(num)
        val_list.append(val)

    length = len(val_list)    
    max_repeat_times, b = max_and_min_num_in_input_list(val_list, length)    #from the above max n min function
    result_index = val_list.index(max_repeat_times)
    return num_list[result_index]

list_a = [1,2,3,4,2,3,1,4,2,3,3,4,2,2,44,44,44,44,44,44,4,4]
mood = most_repeat_num(list_a)
print(f"{mood} is the most repeated number")


